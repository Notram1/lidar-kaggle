# 17_Lidar_Kaggle

### Lyft dataset structure

The dataset consists of json files (beside the image, lidar and map files):

1. `scene.json` - 25-45 seconds snippet of a car's journey.
2. `sample.json` - An annotated snapshot of a scene at a particular timestamp.
3. `sample_data.json` - Data collected from a particular sensor.
4. `sample_annotation.json` - An annotated instance of an object within our interest.
5. `instance.json` - Enumeration of all object instance we observed.
6. `category.json` - Taxonomy of object categories (e.g. vehicle, human).
7. `attribute.json` - Property of an instance that can change while the category remains the same.
8. `visibility.json` - (currently not used)
9. `sensor.json` - A specific sensor type.
10. `calibrated_sensor.json` - Definition of a particular sensor as calibrated on a particular vehicle.
11. `ego_pose.json` - Ego vehicle poses at a particular timestamp.
12. `log.json` - Log information from which the data was extracted.
13. `map.json` - Map data that is stored as binary semantic masks from a top-down view.

## Convert lyft to KITTI

You can use pip to install [lyft-dataset-sdk](https://pypi.org/project/lyft-dataset-sdk/):
```bash
pip install -U lyft_dataset_sdk
```

If you want to get the latest version of the code before it is released on PyPI you can install the library from GitHub:

```bash
pip install -U git+https://github.com/lyft/nuscenes-devkit
```
This will install all dependencies.

### Dataset Download
Go to <https://level5.lyft.com/dataset/> to download the Lyft Level 5 AV Dataset.

The dataset is also availible as a part of the [Lyft 3D Object Detection for Autonomous Vehicles Challenge](https://www.kaggle.com/c/3d-object-detection-for-autonomous-vehicles).

### Utils for converting LEVEL5 data into Kitti format
Simply run<br />`python -m lyft_dataset_sdk.utils.export_kitti nuscenes_gt_to_kitti --lyft_dataroot ${DS_PTH} --table_folder ${TBL_PTH}`<br />for converting data.
<br />See help ( `python -m lyft_dataset_sdk.utils.export_kitti nuscenes_gt_to_kitti --help` ) for more information.
<br />You can draw results after converting with utils:<br />`python -m lyft_dataset_sdk.utils.export_kitti render_kitti`


## Complex YOLO
1. You can download the data from officia website of kitti
* <http://www.cvlibs.net/download.php?file=data_object_velodyne.zip>
* <http://www.cvlibs.net/download.php?file=data_object_label_2.zip>
* <http://www.cvlibs.net/download.php?file=data_object_calib.zip>

2. Create the following folder structure in the current working directory

    tensorflow_complex_yolo/kitti/training/calib,label_2,velodyne


3. Unzip the downloaded kitti dataset, and place them in the corresponding folder created above

4. Then create RGB-image dataset:
```bash
python utils/make_image_dataset.py
```

This script will convert the point cloud data into image data, which will be automatically saved in the ./kitti/image_dataset/, and will generate test_image_list.txt and train_image_list.txt in the ./config folder.

5. Train model

```bash
python train.py 
         --load_weights 
         --weights_path
         --batch_size
         --num_iter
         --save_dir
         --save_interval
         --gpu_id
```


All parameters have default values, but you can easily change them. For example, if you want to set the number of iteration to 10, write 10 after --num_iter.

6. Prediction

```bash
python predict.py  --weights_path =./weights_path/...  --draw_gt_box=True
```

When runnig predict.py the script saves predicted results in predict_result folder.

7. Results

* with default parameters

![](src/yolo/Tensorflow_complex_yolo/results/result.png)

* with lyft to kitti converted datas

![](src/yolo/Tensorflow_complex_yolo/results/000000.png)

Regarding the second prediction, our opinion is that the result shows a motion prediction, not a moment prediction. Also, the location of the sensor in the case of the original KITTI dataset is in the upper middle of the picture (lidar with 180&deg; field of view), while the lyft dataset default origin is in the center of the picture (lidar with 360&deg; field of view). These issues justify the need of rethinking the processes of the YOLO algorithm and lyft dataset conversion. 


                                    